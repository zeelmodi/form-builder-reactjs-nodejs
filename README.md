# Form Builder using Redux and Redux-saga

Users can create dynamic forms using Form Builder application like google forms.

## Installation

### Downloading and installing steps:

- **[Download or Clone](https://gitlab.com/zeelmodi/form-builder-reactjs-nodejs.git)** the latest version of the Code.

### Client Side

```bash
cd form-builder/frontend
npm install
npm start
```

### Server Side

```bash
cd form-builder/server
npm install
npm start
```

### Database

##### Note: Default Frontend port is 3000 and backend port is 4000
