import {
    CREATE_FORM, CREATE_FORM_FAIL, CREATE_FORM_SUCCESS, GET_FORMS, GET_FORMS_FAIL, GET_FORMS_SUCCESS,
    GET_FORM_DETAILS, GET_FORM_DETAILS_FAIL, GET_FORM_DETAILS_SUCCESS,
    ADD_RESPONSE, ADD_RESPONSE_FAIL, ADD_RESPONSE_SUCCESS
} from './actionTypes';

export const createForm = (formData) => {
    return {
        type: CREATE_FORM,
        payload: formData
    }
}

export const createFormSuccess = (form) => {
    return {
        type: CREATE_FORM_SUCCESS,
        payload: form
    }
}

export const createFormFail = (error) => {
    return {
        type: CREATE_FORM_FAIL,
        payload: error
    }
}

export const getForms = () => {
    return { type: GET_FORMS }
};

export const getFormsSuccess = (forms) => {
    return {
        type: GET_FORMS_SUCCESS,
        payload: forms
    }
}

export const getFormsFail = (error) => {
    return {
        type: GET_FORMS_FAIL,
        payload: error
    }
}

export const getFormDetails = (id) => {
    return {
        type: GET_FORM_DETAILS,
        payload: id
    }
}

export const getFormDetailSuccess = (form) => {
    return {
        type: GET_FORM_DETAILS_SUCCESS,
        payload: form
    }
}

export const getFormDetailFail = (error) => {
    return {
        type: GET_FORM_DETAILS_FAIL,
        payload: error
    }
}

export const addResponse = (responseData) => {
    return {
        type: ADD_RESPONSE,
        payload: responseData
    }
}

export const addResponseSuccess = (response) => {
    return {
        type: ADD_RESPONSE_SUCCESS,
        payload: response
    }
}

export const addResponseFail = (error) => {
    return {
        type: ADD_RESPONSE_FAIL,
        payload: error
    }
}