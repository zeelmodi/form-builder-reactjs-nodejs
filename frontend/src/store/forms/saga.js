import { takeLatest, put, call } from "redux-saga/effects";

import {
    CREATE_FORM, GET_FORMS,
    GET_FORM_DETAILS,
    ADD_RESPONSE
} from './actionTypes';

import {
    createFormFail, createFormSuccess,
    getFormDetailFail, getFormDetailSuccess,
    getFormsFail, getFormsSuccess,
    addResponseFail, addResponseSuccess
} from './actions';

import { createForm, getFormDetails, getForms } from '../../helpers/form_helper';
import { submitResponses } from '../../helpers/response_helper';

function* onGetForms() {
    try {
        const response = yield call(getForms);
        yield put(getFormsSuccess(response));
    } catch (error) {
        yield put(getFormsFail(error));
    }
}

function* onGetFormDetails({ payload: id }) {
    try {
        const response = yield call(getFormDetails, id);
        yield put(getFormDetailSuccess(response));
    } catch (error) {
        yield put(getFormDetailFail(error));
    }
}

function* onCreateForm({ payload: formBody }) {
    try {
        const response = yield call(createForm, formBody);
        yield put(createFormSuccess(response));
    } catch (error) {
        yield put(createFormFail(error));
    }
}

function* onSubmitResponse({ payload: responseData }) {
    try {
        const response = yield call(submitResponses, responseData);
        yield put(addResponseSuccess(response));
    } catch (error) {
        yield put(addResponseFail(error));
    }
}

function* FormSaga() {
    yield takeLatest(GET_FORMS, onGetForms);
    yield takeLatest(GET_FORM_DETAILS, onGetFormDetails);
    yield takeLatest(CREATE_FORM, onCreateForm);
    yield takeLatest(ADD_RESPONSE, onSubmitResponse);
}

export default FormSaga;