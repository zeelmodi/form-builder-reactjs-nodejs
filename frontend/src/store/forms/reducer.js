import {
    CREATE_FORM, CREATE_FORM_FAIL, CREATE_FORM_SUCCESS, GET_FORMS, GET_FORMS_FAIL, GET_FORMS_SUCCESS,
    GET_FORM_DETAILS, GET_FORM_DETAILS_FAIL, GET_FORM_DETAILS_SUCCESS,
    ADD_RESPONSE, ADD_RESPONSE_FAIL, ADD_RESPONSE_SUCCESS
} from './actionTypes';

const initialState = {
    forms: [],
    form: {},
    formBody: {},
    loadingCreateForm: false,
    loadingForms: false,
    loadingFormDetails: false,
    responseData: {},
    loadingResponse: false,
    error: {}
}

const FormReducer = (state = initialState, action) => {
    switch (action.type) {
        case CREATE_FORM:
            state = { ...state, loadingCreateForm: true };
            break;

        case CREATE_FORM_FAIL:
            state = { ...state, error: action.payload, loadingCreateForm: false };
            break;

        case CREATE_FORM_SUCCESS:
            state = { ...state, form: action.payload, loadingCreateForm: false };
            break;

        case GET_FORMS:
            state = { ...state, loadingForms: true };
            break;

        case GET_FORMS_FAIL:
            state = { ...state, error: { message: "Error" }, loadingForms: false };
            break;

        case GET_FORMS_SUCCESS:
            state = { ...state, forms: action.payload, loadingForms: false };
            break;

        case GET_FORM_DETAILS:
            state = { ...state, loadingFormDetails: true };
            break;

        case GET_FORM_DETAILS_FAIL:
            state = { ...state, error: { message: "Error" }, loadingFormDetails: false };
            break;

        case GET_FORM_DETAILS_SUCCESS:
            state = { ...state, form: action.payload, loadingFormDetails: false };
            break;

        case ADD_RESPONSE:
            state = { ...state, loadingResponse: true };
            break;

        case ADD_RESPONSE_SUCCESS:
            state = { ...state, responseData: action.payload, loadingResponse: false };
            break;

        case ADD_RESPONSE_FAIL:
            state = { ...state, error: action.payload, loadingResponse: false };
            break;

        default:
            state = { ...state };
            break;
    }
    return state;
}

export default FormReducer