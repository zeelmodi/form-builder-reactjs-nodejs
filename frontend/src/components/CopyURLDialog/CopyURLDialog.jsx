import React from 'react'
import {
    Dialog, DialogTitle, DialogContent, Paper, Grid, Typography,
    IconButton, DialogContentText, DialogActions, Button,
} from '@mui/material';
import CopyIcon from '@mui/icons-material/ContentCopy';



const CopyURLDialog = ({ openDialog, handleClose, data, }) => {

    // const classes = useStyles();

    const clipToClipboard = () => {
        navigator.clipboard.writeText(window.location.origin + "/form/s/" + data)
    }
    return (
        <>
            <Dialog
                open={openDialog}
                onClose={handleClose}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
              
            >
                <DialogTitle id="alert-dialog-title">{"Copy and share link."}</DialogTitle>
                <DialogContent>
                    <Paper
                        
                    >
                        <Grid container alignContent="space-between" alignItems="center">
                            <Grid item>
                                <Typography variant="body1">{window.location.origin + "/form/s/" + data}</Typography>
                          
                            </Grid>
                            <Grid item alignItems="flex-end">
                                <IconButton  aria-label="Add" size="medium"
                                    onClick={clipToClipboard}

                                >
                                    <CopyIcon />
                                </IconButton>
                            </Grid>
                        </Grid>
                    </Paper>                                    
                    <DialogContentText id="alert-dialog-description">
                  
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} color="primary">
                        Cancel
                    </Button>
                
                </DialogActions>
            </Dialog>
        </>
    )
};

export default CopyURLDialog
