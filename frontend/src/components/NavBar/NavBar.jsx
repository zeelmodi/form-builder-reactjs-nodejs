import React from 'react'

import { AppBar, Box, Button, IconButton, Toolbar, Typography } from '@mui/material';

import HomeIcon from '@mui/icons-material/Home';
import AddIcon from '@mui/icons-material/Add';
import { useHistory, useLocation } from 'react-router-dom';

const NavBar = () => {

    const history = useHistory();
    const location = useLocation();

    const onAddForm = () => history.push('/add-form');

    return (
        <Box sx={{ flexGrow: 1 }}>
            <AppBar position="static">
                <Toolbar>
                    <IconButton
                        size="large"
                        edge="start"
                        color="inherit"
                        aria-label="menu"
                        sx={{ mr: 2 }}
                    >
                        <HomeIcon />
                    </IconButton>
                    <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
                        Form Builder
                    </Typography>
                    
                    {location.path !== '/add-form' && (<Button color="inherit" startIcon={<AddIcon />} onClick={onAddForm} >Add Form</Button>)}                
                    
                </Toolbar>
            </AppBar>
        </Box>
    )
};

export default NavBar
