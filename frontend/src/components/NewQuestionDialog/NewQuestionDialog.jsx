import React, { useEffect, useState } from 'react'
import {
    Box, Dialog, DialogContent,
    DialogContentText, DialogTitle, FormControl, FormControlLabel, InputLabel,
    MenuItem, Select, TextField, Radio, Checkbox, DialogActions, Button,
} from '@mui/material'

const NewQuestionDialog = ({ questionDialog, handleClose, onAddQuestion }) => {

    const [question, setquestion] = useState('')
    const [answerType, setanswerType] = useState('');
    const [options, setoptions] = useState([]);

    const [formValid, setFormValid] = useState(false);

    const changeAnswerType = (event) => {
        setanswerType(event.target.value);
    }

    function addOption() {
        setoptions([...options, {optionText: `Option ${options.length + 1}`}]);
    }

    const submitQuestion = () => {        
        onAddQuestion({question, answerType, options})
    }

    function handleOptionValue(text, i) {
        const newOptions = [...options];
        newOptions[i].optionText = text;
        setoptions(newOptions);
    }

    const isFormValid = () => {
        if (!question || !answerType) { setFormValid(true); return; }
        if ((answerType === 'radio' || answerType === 'checkbox') && !options.length) { setFormValid(true); return; }
        setFormValid(false);
    };

    useEffect(() => {
        if (answerType === 'shortText') {
            setoptions([]);
        }
    }, [answerType]);

    useEffect(() => {
        isFormValid();
    }, [question, answerType, options]);

    return (
        <>
            <Dialog open={questionDialog} onClose={handleClose} fullWidth >
                <DialogTitle>Add a question</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        Please add your question here.
                    </DialogContentText>
                    <Box sx={{ mt: 2, mb: 2 }}>
                        <TextField
                            autoFocus
                            margin="none"
                            id="name"
                            label="Question"
                            type="text"
                            fullWidth
                            variant="outlined"
                            value={question}
                            onChange={(e) => setquestion(e.target.value)}
                        />
                    </Box>

                    <Box sx={{ mt: 2, mb: 2 }}>
                        <FormControl fullWidth>
                            <InputLabel id="demo-simple-select-label">Answer Type</InputLabel>
                            <Select
                                labelId="demo-simple-select-label"
                                id="demo-simple-select"
                                value={answerType}
                                label="Answer Type"
                                onChange={changeAnswerType}
                            >
                                <MenuItem value='shortText'>Text</MenuItem>
                                <MenuItem value='radio'>Radio Buttons</MenuItem>
                                <MenuItem value='checkbox'>Checkboxes</MenuItem>
                            </Select>
                        </FormControl>
                    </Box>
                </DialogContent>

                {answerType === 'radio' || answerType === 'checkbox' ? (
                    <>
                        {
                            options.map((option, i) => (
                                <Box sx={{ mt: 2, mb: 2, paddingLeft: 2 }} key={`option-${i}`}>
                                    {answerType === 'radio' ? (< Radio disabled />) : (<Checkbox disabled />)}
                                    <TextField
                                        size="small"
                                        placeholder="Option text"
                                        style={{ marginTop: '5px' }}
                                        value={option.optionText}
                                        onChange={(e) => handleOptionValue(e.target.value, i)}
                                    />
                                </Box>
                            ))
                        }


                        <Box sx={{ mt: 2, mb: 2, paddingLeft: 3 }}>
                            <FormControlLabel disabled control={answerType === 'radio' ? (< Radio disabled />) : (<Checkbox disabled />)} label={
                                <Button size="small"
                                    onClick={() => { addOption() }}
                                    style={{ textTransform: 'none' }}>
                                    Add Option
                                </Button>
                            } />
                        </Box>
                    </>
                ) : ('')}


                <DialogActions>
                    <Button onClick={handleClose}>Cancel</Button>
                    <Button onClick={submitQuestion} disabled={formValid}>Add</Button>
                </DialogActions>
            </Dialog >
        </>
    )
};

export default NewQuestionDialog
