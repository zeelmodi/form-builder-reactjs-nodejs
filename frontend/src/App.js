import React from 'react'
import {
  BrowserRouter as Router,
  Route,
  Switch,

} from 'react-router-dom'

import './App.css';

import AddNewFormPage from './pages/AddNewFormPage';
import DashboardPage from './pages/DashboardPage';
import SubmitFormPage from './pages/SubmitFormPage';

function App() {
  return (
    <div className="App">
      <Router>
        <Switch>
          <Route path="/" exact component={DashboardPage} />
          <Route path="/add-form" exact component={AddNewFormPage} />
          <Route path="/form/s/:slug" exact component={SubmitFormPage} />
        </Switch>
      </Router>

    </div>
  );
}

export default App;
