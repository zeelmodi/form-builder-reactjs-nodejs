import React, { useEffect, useState } from 'react'

import {
    Button, Container, Grid, Paper, Typography,
    RadioGroup, Radio, FormControlLabel, TextField,
    Box, FormControl, FormGroup, Checkbox, CircularProgress
} from '@mui/material'
import { useParams } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

import { getFormDetails } from '../store/forms/actions';
import { addResponse } from '../store/responses/actions';

const SubmitFormPage = () => {

    const { slug } = useParams();

    const { form, loadingFormDetails, responseData: responded, loadingResponse, error } = useSelector((state) => state.FormReducer);

    const dispatch = useDispatch();

    const [isSubmitted, setIsSubmitted] = useState(false);

    const [value, setValue] = useState('');
    const [responseData, setResponseData] = useState([])

    const [formData, setFormData] = useState(null);
    const [questions, setQuestions] = useState([]);

    const [isFormValid, setFormValid] = useState(false)

    useEffect(() => {
        console.log({ responded, loadingResponse });
        if (Object.keys(error).length) {
            console.log({ error });
        }
        if (Object.keys(responded).length) {
            if (responded.success) {
                setIsSubmitted(true)
            }
        }
    }, [responded, loadingResponse, error])


    function reloadForAnotherResponse() {
        window.location.reload(true);
    }

    const handleRadioChange = (j, i) => {
        const questionId = questions[i]._id
        var optionId = questions[i].options[j]._id

        const data = {
            questionId, optionId: [optionId]
        }

        setValue(j)

        const fakeRData = [...responseData];

        const indexOfResponse = fakeRData.findIndex(x => x.questionId === questionId);
        if (indexOfResponse === -1) {

            setResponseData(responseData => [...responseData, data])

        } else {
            fakeRData[indexOfResponse] = data
            setResponseData(fakeRData);
        }

        // setOptionValue(fakeData);
        //  
    };

    const handleCheckBoxChange = (j, i, checked) => {
        const questionId = questions[i]._id
        const optionId = questions[i].options[j]._id;


        const fakeRData = [...responseData];

        const indexOfResponse = fakeRData.findIndex(x => x.questionId === questionId);
        if (indexOfResponse === -1) {
            if (checked) {

                setResponseData(responseData => [...responseData, { questionId, optionId: [optionId] }]);
            }
        }
        else {
            if (checked) {
                fakeRData[indexOfResponse] = {
                    questionId, optionId: [...responseData[indexOfResponse].optionId, optionId]
                };

                setResponseData(fakeRData);
            }
            else {
                const findIndex = responseData[indexOfResponse].optionId.map((x) => x.optionId === optionId);
                const newOptions = responseData[indexOfResponse].optionId.slice(findIndex, 1);
                fakeRData[indexOfResponse] = {
                    questionId, optionId: [...newOptions]
                }

                setResponseData(fakeRData);
            }
        }
    }

    const handleTextChange = (text, i) => {
        const questionId = questions[i]._id;
        const fakeRData = [...responseData];

        const indexOfResponse = fakeRData.findIndex(x => x.questionId === questionId);
        if (indexOfResponse === -1) {

            setResponseData(responseData => [...responseData, { questionId, answerText: text }])

        } else {
            fakeRData[indexOfResponse] = { questionId, answerText: text };
            setResponseData(fakeRData);
        }
    }

    function submitResponse() {
        try {
            dispatch(addResponse({
                formId: formData._id,
                response: responseData
            }));
            // setIsSubmitted(true);
        } catch (error) {
            console.log(error);
        }

    }

    useEffect(() => {
        dispatch(getFormDetails(slug));
    }, [slug]);

    useEffect(() => {
        console.log({ form, loadingFormDetails });
        setFormData(form?.data?.form);
        setQuestions(form?.data?.form?.questions);
    }, [form, loadingFormDetails]);

    useEffect(() => {
        if (!responseData.length) {
            setFormValid(true);
            return;
        }
        else {
            setFormValid(false);
        }
    }, [responseData]);

    return (
        <>
            <Container maxWidth="lg">
                <div style={{ marginTop: '15px', marginBottom: '7px', paddingBottom: "30px" }}>
                    <Grid
                        container
                        direction="column"
                        justify="center"
                        alignItems="center"
                    >
                        {
                            !loadingFormDetails && formData ? (
                                <>
                                    <Grid item xs={12} sm={5} style={{ width: '100%' }}>
                                        {!isSubmitted ? (
                                            <>
                                                <Grid style={{ borderTop: '10px solid teal', borderRadius: 10 }}>
                                                    <div>
                                                        <div>
                                                            <Paper elevation={2} style={{ width: '100%' }}>
                                                                <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'flex-start', marginLeft: '15px', paddingTop: '20px', paddingBottom: '20px' }}>
                                                                    <Typography variant="h4" style={{ fontFamily: 'sans-serif Roboto', marginBottom: "15px" }}>
                                                                        {formData.title}
                                                                    </Typography>
                                                                </div>
                                                            </Paper>
                                                        </div>
                                                    </div>
                                                </Grid>

                                                <div>
                                                    <Grid>
                                                        {questions.map((ques, i) => (
                                                            <div key={i}>
                                                                <br></br>
                                                                <Paper>
                                                                    <div>
                                                                        <div style={{
                                                                            display: 'flex',
                                                                            flexDirection: 'column',
                                                                            alignItems: 'flex-start',
                                                                            marginLeft: '6px',
                                                                            paddingTop: '15px',
                                                                            paddingBottom: '15px'
                                                                        }}>
                                                                            <Typography variant="subtitle1" style={{ marginLeft: '10px' }}>{i + 1}. {ques.question}</Typography>
                                                                            <div>
                                                                                {
                                                                                    ques.answerType === 'shortText' ? (<Box
                                                                                        sx={{
                                                                                            width: 500,
                                                                                            maxWidth: '100%',
                                                                                        }}
                                                                                    >
                                                                                        <TextField fullWidth placeholder='Short Text' value={ques?.answerText} onChange={(e) => handleTextChange(e.target.value, i)} />
                                                                                    </Box>) : (
                                                                                        <>
                                                                                            {ques.answerType === 'radio' && (<RadioGroup aria-label="quiz" name="quiz" value={value}
                                                                                                onChange={(e) => { handleRadioChange(e.target.value, i) }}>

                                                                                                {ques.options.map((op, j) => (
                                                                                                    <div key={j}>
                                                                                                        <div style={{ display: 'flex', marginLeft: '7px' }}>
                                                                                                            <FormControlLabel value={j} control={<Radio />} label={op.optionText} />
                                                                                                        </div>
                                                                                                    </div>
                                                                                                ))}
                                                                                            </RadioGroup>)}

                                                                                            {ques.answerType === 'checkbox' && (
                                                                                                <FormControl >
                                                                                                    <FormGroup onChange={(e) => handleCheckBoxChange(e.target.value, i, e.target.checked)} >
                                                                                                        {ques.options.map((op, j) => (
                                                                                                            <div key={j}>
                                                                                                                <div style={{ display: 'flex', marginLeft: '7px' }}>
                                                                                                                    <FormControlLabel value={j} control={<Checkbox />} label={op.optionText} />
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        ))}
                                                                                                    </FormGroup>
                                                                                                </FormControl>
                                                                                            )}

                                                                                        </>
                                                                                    )
                                                                                }
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </Paper>
                                                            </div>
                                                        ))}
                                                    </Grid>
                                                    <Grid>
                                                        <br></br>
                                                        <div style={{ display: 'flex' }}>
                                                            <Button variant="contained" color="primary" disabled={isFormValid} onClick={submitResponse}>
                                                                Submit
                                                            </Button>
                                                        </div>
                                                        <br></br>

                                                        <br></br>

                                                    </Grid>
                                                </div>
                                            </>
                                        ) :
                                            (
                                                <div>
                                                    <Typography variant="body1">Form submitted</Typography>
                                                    <Typography variant="body2">Thanks for submiting form</Typography>


                                                    <Button onClick={reloadForAnotherResponse}>Submit another response</Button>
                                                </div>
                                            )
                                        }
                                    </Grid>
                                </>) : (<Box sx={{ display: 'flex' }}>
                                    <CircularProgress />
                                </Box>)
                        }
                        {/* {loadingFormData ? (<CircularProgress />):""} */}


                    </Grid>
                </div>
            </Container>
        </>
    )
};

export default SubmitFormPage
