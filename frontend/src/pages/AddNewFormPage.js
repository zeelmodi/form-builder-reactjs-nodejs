import React, { useState, useEffect } from 'react'
import {
    Button,
    Card,
    CardActions,
    CardContent,
    Checkbox,
    CircularProgress,
    Container,
    FormControlLabel, Grid, IconButton, Paper, Radio,
    Box,
    TextField, Typography
} from '@mui/material';
import { useHistory } from 'react-router-dom';

import AddCircleIcon from '@mui/icons-material/AddCircle';
import DeleteOutlineIcon from '@mui/icons-material/DeleteOutline';
import SaveIcon from '@mui/icons-material/Save';

import { useDispatch, useSelector } from "react-redux";

import NavBar from '../components/NavBar';
import NewQuestionDialog from '../components/NewQuestionDialog';
import CopyURLDialog from '../components/CopyURLDialog';

import { createForm } from '../store/forms/actions';



const AddNewFormPage = () => {

    const history = useHistory();

    const dispatch = useDispatch();
    const { form, loadingCreateForm, error } = useSelector((state) => state.FormReducer);
    const [showCopyDialog, setCopyDialog] = useState(false);
    const [formURL, setFormURL] = useState('')

    const [title, setTitle] = useState('Untitled Form');
    const [questions, setQuestions] = useState([]);
    const [questionDialog, setquestionDialog] = useState(false);

    const [isFormSubmitted, setFormSubmitted] = useState(false);

    const handleClickOpen = () => {
        setquestionDialog(true);
    };

    const handleClose = () => {
        setquestionDialog(false);
    };

    const closeCopyDialog = () => {
        setCopyDialog(false);
        history.push('/');
    }

    const openCopyDialog = () => {
        setCopyDialog(true);
    }

    useEffect(() => {
        console.log({ form, loadingCreateForm });
        if (Object.keys(error).length) {
            console.log({ error });
        }
        if (Object.keys(form).length) {

            setFormURL(form.data.form.slug)
            openCopyDialog()
        }

    }, [form, loadingCreateForm, error]);

    function resetForm() {
        setTitle('Untitled Form');
        setQuestions([]);
        setFormURL('');
        setFormSubmitted(false);
    }

    const onSubmitForm = () => {
        console.log({ title, questions });

        setFormSubmitted(true);
        try {
            dispatch(createForm({ title, questions }));

        } catch (error) {
            console.log(error);
        }
        finally {
            setFormSubmitted(false);
        }

    }

    function deleteQuestion(i) {
        const qs = [...questions];
        if (questions.length > 1) {
            qs.splice(i, 1);
        }
        setQuestions(qs);
    }

    const onAddQuestion = (data) => {
        setQuestions(questions => [...questions, data]);
        handleClose();
    }

    const AddQuestions = () => {
        return questions.map((ques, i) => (
            <>
                <div>
                    <div>
                        <div style={{ marginBottom: "15px" }}>
                            <Card
                                expanded={questions[i].open}>
                                <CardContent
                                    aria-controls="panel1a-content"
                                    id="panel1a-header"
                                    elevation={1} style={{ width: '100%' }}
                                >

                                    <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'flex-start', marginLeft: '3px', paddingTop: '15px', paddingBottom: '15px' }}>
                                        {/* <TextField id="standard-basic" label=" " value="Question" InputProps={{ disableUnderline: true }} />  */}

                                        <Typography variant="subtitle1" style={{ marginLeft: '0px' }}>{i + 1}.  {ques.question}</Typography>
                                        {ques.answerType === 'shortText' ? (<TextField
                                            size="medium"
                                            placeholder="Short answer"
                                            style={{ marginTop: '5px' }}
                                            disabled
                                        />) : (
                                            <>
                                                {ques.options.map((op, j) => (

                                                    <div key={j}>
                                                        <div style={{ display: 'flex' }}>
                                                            <FormControlLabel disabled
                                                                control={
                                                                    op.answerType === 'radio' ?
                                                                        (<Radio style={{ marginRight: '3px', }} />) :
                                                                        (<Checkbox style={{ marginRight: '3px', }} />)}
                                                                label={
                                                                    <Typography style={{ color: '#555555' }}>
                                                                        {ques.options[j].optionText}
                                                                    </Typography>
                                                                } />
                                                        </div>

                                                    </div>
                                                ))}
                                            </>
                                        )}
                                    </div>
                                </CardContent>

                                <CardActions>
                                    <IconButton
                                        aria-label="delete"
                                        onClick={() => { deleteQuestion(i) }}
                                    >
                                        <DeleteOutlineIcon />
                                    </IconButton>

                                </CardActions>
                            </Card>
                        </div>
                    </div>
                </div>
            </>
        ));
    };

    return (
        <>
            <NavBar />
            <Container maxWidth="lg">
                <div style={{ marginTop: '15px', marginBottom: '7px', paddingBottom: "30px" }}>
                    <Grid
                        container
                        direction="column"
                        justify="center"
                        alignItems="center"
                    >
                        {/* {loadingFormData ? (<CircularProgress />):""} */}
                        {
                            isFormSubmitted ? (<>
                                {loadingCreateForm && (
                                    <Box sx={{ display: 'flex' }}>
                                        <CircularProgress />
                                    </Box>)}
                            </>) : (
                                <>
                                    <Grid item xs={12} sm={5} style={{ width: '100%' }}>

                                        <Grid style={{ borderTop: '10px solid teal', borderRadius: 10 }}>
                                            <div>
                                                <div>
                                                    <Paper elevation={2} style={{ width: '100%' }}>
                                                        <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'flex-start', marginLeft: '15px', paddingTop: '20px', paddingBottom: '20px' }}>
                                                            {/* <Typography variant="h4" style={{ fontFamily: 'sans-serif Roboto', marginBottom: "15px" }}>
                                                    Untitled Form
                                                </Typography> */}
                                                            <TextField
                                                                autoFocus
                                                                margin="none"
                                                                id="name"
                                                                label="Title"
                                                                type="text"
                                                                fullWidth
                                                                variant="filled"
                                                                value={title}
                                                                onChange={(e) => setTitle(e.target.value)}
                                                            />
                                                        </div>
                                                    </Paper>
                                                </div>
                                            </div>
                                        </Grid>

                                        {questionDialog && <NewQuestionDialog questionDialog={questionDialog} handleClose={handleClose} onAddQuestion={onAddQuestion} />}
                                        {showCopyDialog && <CopyURLDialog data={formURL}
                                            handleClose={closeCopyDialog} openDialog={showCopyDialog} />}

                                        <Grid style={{ paddingTop: '10px' }}>
                                            <AddQuestions />
                                            <div>
                                                <Button
                                                    variant="contained"
                                                    onClick={handleClickOpen}
                                                    // onClick={addMoreQuestionField}
                                                    endIcon={<AddCircleIcon />}
                                                    style={{ margin: '5px' }}
                                                >Add Question </Button>
                                                <Button
                                                    variant="contained"
                                                    onClick={onSubmitForm}
                                                    // onClick={addMoreQuestionField}
                                                    endIcon={<SaveIcon />}
                                                    style={{ margin: '5px' }}
                                                >Save Form </Button>
                                            </div>
                                        </Grid>
                                    </Grid>
                                </>
                            )
                        }


                    </Grid>
                </div>

            </Container >
        </>
    )
};

export default AddNewFormPage;
