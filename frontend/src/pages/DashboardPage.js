import React, { useEffect, } from 'react';
import { Link } from 'react-router-dom';
import {
    CircularProgress,
    Container, Paper, Table, TableBody, TableCell,
    TableContainer, TableHead, TableRow, Typography,
    Box
} from '@mui/material';

import { useDispatch, useSelector } from "react-redux";

import NavBar from '../components/NavBar';

import { REACT_APP_APP_URL } from '../helpers/api_helper';
import { getForms } from '../store/forms/actions';


const DashboardPage = () => {

    const dispatch = useDispatch();
    const { forms, loadingForms } = useSelector((state) => state.FormReducer);

    useEffect(() => {
        dispatch(getForms());
    }, []);

    useEffect(() => {
        console.log({ forms, loadingForms });
    }, [forms, loadingForms])

    return (
        <>
            <NavBar />
            <Container maxWidth="lg">
                <Typography pt="2" pb="2" variant="h3" gutterBottom component="div">Recent Forms</Typography>
                {
                    !loadingForms ? (
                        <TableContainer component={Paper} >
                            <Table sx={{ minWidth: 650 }} aria-label="simple table">
                                <TableHead>
                                    <TableRow>
                                        <TableCell>Form Name</TableCell>
                                        <TableCell >Form URL</TableCell>
                                        <TableCell >Created At</TableCell>
                                        <TableCell align="right">Total Responses</TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {forms?.data?.forms.map((row) => (
                                        <TableRow
                                            key={row.title}
                                            sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                        >
                                            <TableCell component="th" scope="row">
                                                {row.title}
                                            </TableCell>
                                            <TableCell>
                                                <Link
                                                    target="_blank"
                                                    rel="noopener"
                                                    to={`/form/s/${row.slug}`}
                                                >{`${REACT_APP_APP_URL}/form/s/${row.slug}`}
                                                </Link>
                                            </TableCell>
                                            <TableCell>{row.createdAt}</TableCell>
                                            <TableCell align="right">{row.totalResponses}</TableCell>
                                        </TableRow>
                                    ))}
                                </TableBody>
                            </Table>
                        </TableContainer>
                    ) : (
                        <Box sx={{ display: 'flex' }}>
                            <CircularProgress />
                        </Box>
                    )
                }

            </Container>
        </>
    )
}

export default DashboardPage
