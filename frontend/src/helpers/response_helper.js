import { post } from './api_helper';
import * as url from "./url_helper";

export const submitResponses = (body) => {
    return post(url.RESPONSES, {}, body)
};