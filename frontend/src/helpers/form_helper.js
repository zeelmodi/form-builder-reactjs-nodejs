import { get, post } from './api_helper';
import * as url from "./url_helper";

export const getForms = () => get(url.FORMS);

export const getFormDetails = (id) => get(`${url.FORMS}/${id}`, {});

export const createForm = (body) => post(url.FORMS, {}, body);