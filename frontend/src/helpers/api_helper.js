import axios from 'axios';

export const REACT_APP_APP_URL = process.env.REACT_APP_APP_URL || 'http://localhost:4000';

const axiosAPI = axios.create({
    baseURL: REACT_APP_APP_URL
});

axios.interceptors.request.use((config) => config);

axios.interceptors.response.use((response) => response, (error) => Promise.reject(error));

export async function get(url, config) {
    return await axiosAPI.get(url, { ...config }).then((response) => response.data);
}

export async function post(url, config, body) {
    return await axiosAPI.post(url, body, { ...config }).then((response) => response.data);
}