const path = require('path');
const fs = require('fs');

// glob.sync(path.join(__dirname, '/**/*.js')).forEach(function (file) {
//     require(path.resolve(file));
// });

const modelsPath = path.resolve(__dirname, '.');

fs.readdirSync(modelsPath).forEach(
    (file) => require(modelsPath + '/' + file)
);