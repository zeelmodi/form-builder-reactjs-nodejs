const mongoose = require('mongoose');

const { Schema } = mongoose;

const optionsRequired = () => {
    return this.answerType !== 'text'
}

const OptionsSchema = new Schema({
    optionText: {
        type: String,
    }
})

const QuestionSchema = new Schema({
    question: {
        type: String,
        required: true,
    },
    answerType: {
        type: String,
        enum: ['shortText', 'radio', 'checkbox'],
        required: true
    },
    options: {
        type: [OptionsSchema],
        required: [optionsRequired, 'Please provide options!'],
    }
})

const FormSchema = new Schema({
    title: {
        type: String,
        required: true,
        trim: true
    },
    slug: {
        type: String,
        required: true,
        trim: true
    },
    questions: {
        type: [QuestionSchema],
        validate: {
            validator: function (value) {
                return value.length >= 1;
            },
            message: 'Please provide at least one question!'
        }
    },

}, { timestamps: true });

FormSchema.index({ title: 1 }, { unique: 'Form name already exists!' });
FormSchema.index({ slug: 1 }, { unique: 'Form name already exists!' });

const Form = mongoose.model('Form', FormSchema);
module.exports = Form;