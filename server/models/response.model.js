const mongoose = require('mongoose');

const { Schema } = mongoose;

const ResponseSchema = new Schema({
    formId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Form',
        required: true
    },
    response: [{
        questionId: {
            type: mongoose.Schema.Types.ObjectId,
            required: true
        },
        answerText: {
            type: String
        },
        optionId: [{ type: mongoose.Schema.Types.ObjectId }]
    }]
}, { timestamps: true });

const Response = mongoose.model('Response', ResponseSchema);
module.exports = Response;