const mongoose = require('mongoose');

const DB_URL = process.env.DB_URL || 'mongodb://localhost:27017/google-form-builder';

mongoose.connect(DB_URL, { useNewUrlParser: true, useUnifiedTopology: true });

const db = mongoose.connection;
db.once('open', () => {
    console.log(`Database connected on with ${DB_URL}`);
});

db.on('error', (err) => {
    console.error('connection error:', err);
});
