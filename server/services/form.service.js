exports.generateSlugName = (title) => {
    const formattedSlug = title.split(' ').join('-').toLowerCase();
    // console.log({ title, formattedSlug });
    return formattedSlug;
}