const { v4: uuidv4 } = require('uuid');

const Form = require("../models/form.model");
const { generateSlugName } = require("../services/form.service");

exports.create = async (req, res, next) => {
    try {

        const slug = uuidv4();

        req.body.slug = slug;

        let form = await new Form(req.body);
        const err = form.validateSync([
            'title',
            'slug',
            'questions'
        ]);

        if (err) return res.status(400).json({
            success: false,
            message: 'Validation Error!',
            err
        });

        form = await form.save();

        return res.status(201).json({
            success: true,
            data: {
                form
            }
        });
    } catch (error) {
        return res.status(500).json({
            success: false,
            error
        });
    }
};

exports.getAllForms = async (req, res, next) => {
    try {
        const forms = await Form.aggregate([
            {
                '$lookup': {
                    'from': 'responses',
                    'localField': '_id',
                    'foreignField': 'formId',
                    'as': 'responses'
                }
            }, {
                '$addFields': {
                    'totalResponses': {
                        '$size': '$responses'
                    }
                }
            }, {
                '$project': {
                    'responses': 0
                }
            }
        ]);

        return res.status(200).json({
            success: true,
            data: {
                forms
            }
        })
    } catch (error) {
        return res.status(500).json({
            success: false,
            error
        });
    }
}

/**
 * 
 */

exports.getFormBySlug = async (req, res, next) => {
    try {
        if (!req.params.slug) {
            return res.status(400).json({
                success: false,
                message: 'Please provide a valid slug.',
            });
        }

        const form = await Form.findOne({ slug: req.params.slug });

        if (!form) {
            return res.status(404).json({
                success: false,
                message: 'Form not found!',
            });
        }

        return res.status(200).json({
            success: true,
            data: {
                form
            }
        });
    } catch (error) {
        return res.status(500).json({
            success: false,
            error
        });
    }
}