const Response = require("../models/response.model");

exports.create = async (req, res, next) => {
    try {
        console.log('Response BODY:', req.body);

        let response = await new Response(req.body);
        const err = response.validateSync([
            'formId',
            'response',
        ]);

        if (err) return res.status(400).json({
            success: false,
            message: 'Validation Error!',
            err
        });

        response = await response.save();

        return res.status(201).json({
            success: true,
            data: {
                response
            }
        });
    } catch (error) {
        return res.status(500).json({
            success: false,
            error
        });
    }
}
