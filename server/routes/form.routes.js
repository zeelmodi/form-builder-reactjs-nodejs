const express = require('express');
const { create, getAllForms, getFormBySlug } = require('../controllers/form.controller');

const router = express.Router();

/* Create a new Form. */
router.post('/', create);
router.get('/', getAllForms);
router.get('/:slug', getFormBySlug);

module.exports = router;
