const express = require('express');
const { create } = require('../controllers/response.controller');

const router = express.Router();

router.post('/', create);

module.exports = router;